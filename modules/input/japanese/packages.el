;; -*- no-byte-compile: t; -*-
;;; input/japanese/packages.el

(package! migemo :pin "f42832c8ac462ecbec9a16eb781194f876fba64a")
(package! avy-migemo :pin "922a6dd82c0bfa316b0fbb56a9d4dd4ffa5707e7")
(unless (featurep! +mozc)
  (package! ddskk :pin "275a831be77573470309a78967734d2b6d10f218"))
(package! pangu-spacing :pin "f92898949ba3bf991fd229416f3bbb54e9c6c223")

(when (featurep! +mozc)
  (package! mozc
    :recipe
    (:host github
     :repo "google/mozc"
     :files ("src/unix/emacs/mozc.el"))
    :pin "e769997e0eff37676c78fb53b122ff79b2fb99b2"))
